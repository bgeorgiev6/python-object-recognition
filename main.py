

from math import fabs
import cv2
import numpy as np

message =""

#Priority road
#img = cv2.imread('download.png')

#stop
#img = cv2.imread('download.jpg')

#pedestrian
img = cv2.imread('download3.png')

#no parking
#img = cv2.imread('download4.jfif')

#define kernel size  
kernel = np.ones((7,7),np.uint8)


# convert to hsv colorspace 
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)


# lower bound and upper bound for Green color 
# lower_bound = np.array([50, 20, 20])     
# upper_bound = np.array([100, 255, 255])

# lower bound and upper bound for Yellow color 
#lower_bound = np.array([20, 80, 80])     
#upper_bound = np.array([30, 255, 255])
def checkBlue(blue):
  blue = False; 
  lower_bound = np.array([101,50,38])
  upper_bound = np.array([110,255,255])
  mask = cv2.inRange(hsv, lower_bound, upper_bound)
  a = mask.max();
  if a == 255:
   blue = 1;
  return blue
     

def checkRed(red):
  red = False; 
  lower_bound = np.array([0, 100, 100])     
  upper_bound = np.array([20, 255, 255])
  mask = cv2.inRange(hsv, lower_bound, upper_bound)
  a = mask.max();
  if a == 255:
     red = 1;
     
     return red
     

def checkYellow(yellow):
 
  lower_bound = np.array([20, 80, 80])     
  upper_bound = np.array([30, 255, 255])
  mask = cv2.inRange(hsv, lower_bound, upper_bound)
  a = mask.max();
  
  if a == 255:
        yellow = 1;
       
        return yellow
              



#upper and lower for Red
yellow = 0; 
red =0;
blue = 0;
r = checkRed(red);
y = checkYellow(yellow);
b = checkBlue(blue)
if y == 1:
    lower_bound = np.array([20, 80, 80])     
    upper_bound = np.array([30, 255, 255])
    mask = cv2.inRange(hsv, lower_bound, upper_bound)
    message = "Priority Road";
    print("Traffic sign detected")  
elif b == 1 and r==1:
  lower_bound = np.array([101,50,38])
  upper_bound = np.array([110,255,255]) 
  lower_bound1 = np.array([0, 100, 100])     
  upper_bound1 = np.array([20, 255, 255]) 
  mask1 = cv2.inRange(hsv, lower_bound, upper_bound)
  mask2 = cv2.inRange(hsv, lower_bound1, upper_bound1)
  mask = mask = cv2.bitwise_xor(mask1, mask2)
  message = "No Parking"

  print("Traffic sign detected")      
elif r == 1:
  lower_bound = np.array([0, 100, 100])     
  upper_bound = np.array([20, 255, 255]) 
  mask = cv2.inRange(hsv, lower_bound, upper_bound)
  message = "Stop sign"
  print("Traffic sign detected") 
elif b == 1:
  lower_bound = np.array([101,50,38])
  upper_bound = np.array([110,255,255]) 
  mask = cv2.inRange(hsv, lower_bound, upper_bound)
  message = "Pedestrian"
  print("Traffic sign detected")  

# find the colors within the boundaries


# Remove unnecessary noise from mask

mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)


#cv2.imshow(message, mask)
# Segment only the detected region

segmented_img = cv2.bitwise_and(img, img, mask=mask)

# Find contours from the mask

contours, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)




# Draw contour on original image

output = cv2.drawContours(img, contours, -1, (0, 255, 0), 6)

# Showing the output

#cv2.imshow("Image", img)
cv2.imshow(message, output)

cv2.waitKey(0)
cv2.destroyAllWindows()

